import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DeckService } from 'src/app/service/deck.service';
import { Deck, Card } from 'src/app/model/card.model';
import { PlayerService } from 'src/app/service/player.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-battle-scene',
  templateUrl: './battle-scene.component.html',
  styleUrls: ['./battle-scene.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BattleSceneComponent implements OnInit {

  public koalaDeck: Deck;
  public sophieDeck: Deck;
  public playedCards: Card[];
  public currentHealth: number;
  public maxHealth: number;

  constructor(
    private deckService: DeckService,
    private playerService: PlayerService
  ) {
    this.playedCards = [];
  }

  ngOnInit(): void {
    this.koalaDeck = this.deckService.getKoalaDeck();
    this.sophieDeck = this.deckService.getSophieDeck();
    this.currentHealth = this.playerService.getCurrentHealth();
    this.maxHealth = this.playerService.getMaxHealth();
  }

  dropInList(event: CdkDragDrop<Card[]>) {
    if (event.container === event.previousContainer) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    }
  }

  dropInPlayArea(event: CdkDragDrop<Card[]>) {
    transferArrayItem(event.previousContainer.data,
      this.playedCards,
      event.previousIndex,
      this.playedCards.length);
    const playedCard = event.item.data;
    this.monsterReact(playedCard);
  }

  monsterReact(card: Card) {
    console.log("Player played " + card.name);
    console.log("Monster reacts with GRRRRRR");
  }
}
