import { Injectable } from '@angular/core';
import { Card, Deck } from '../model/card.model';

@Injectable({
  providedIn: 'root'
})
export class DeckService {

  private koalaStartingDeck: Deck = [
    {
      name: "Card 1",
      text: "This is a test"
    },
    {
      name: "Card 2",
      text: "This is a test"
    },
    {
      name: "Card 3",
      text: "This is a test"
    },
    {
      name: "Card 4",
      text: "This is a test"
    },
    {
      name: "Card 5",
      text: "This is a test"
    }
  ]

  private sophieStartingDeck: Deck = [
    {
      name: "Card 6",
      text: "This is a test"
    },
    {
      name: "Card 7",
      text: "This is a test"
    },
    {
      name: "Card 8",
      text: "This is a test"
    },
    {
      name: "Card 9",
      text: "This is a test"
    },
    {
      name: "Card 10",
      text: "This is a test"
    }
  ]

  private koalaDeck: Deck;
  private sophieDeck: Deck;

  constructor() {
    this.koalaDeck = this.koalaStartingDeck;
    this.sophieDeck = this.sophieStartingDeck;
  }

  public getKoalaDeck() {
    return this.koalaDeck;
  }

  public getSophieDeck() {
    return this.sophieDeck;
  }
}
