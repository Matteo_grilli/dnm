import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor() { }

  public getCurrentHealth() {
    return 30;
  }

  public getMaxHealth() {
    return 50;
  }
}
