export interface Card {
  name: string;
  text: string;
}

export type Deck = Card[];
export const MAX_DECK_SIZE = 5;
